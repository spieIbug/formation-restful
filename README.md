to run this project you'll need to :
- run ```npm install``` on the root folder
- after that access to frontend folder and run ```bower install```
- then you'll create your database, and import the [file](./backend/db.sql)
- to update database settings goto this [file](./backend/common/dbc.js)
> note that users passwords are not hashed, it recommanded to hash them if you want to use this code.
