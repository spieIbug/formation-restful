/**
 * Created by yacmed on 04/03/2016.
 */
"use strict";
var express 	= require('express'),
    bodyParser  = require('body-parser'),
    cookieParser  = require('cookie-parser'),
    expressSession  = require('express-session'),
    http        = require('http'),
    path        = require('path'),
    morgan = require('morgan'),
    passport = require('passport'),
    passportLocal = require('passport-local');
/**
 * APIS
 * @type {localStrategy|exports|module.exports}
 */
var LocalStrategy = require('./backend/security/local-strategy');
var partials  = require('./backend/routes/partials');
var usersApi 		= require('./backend/dao/users-dao'),
    userRolesApi 		= require('./backend/dao/users-roles-dao');
var charges  = require('./backend/routes/charges'),
    ressources  = require('./backend/routes/ressources'),
    modesPaiements = require('./backend/routes/modes-paiements'),
    modesGestionRessources = require('./backend/routes/modes-gestions-ressources'),
    typesTiersPayeurs = require('./backend/routes/types-tiers-payeurs'),
    famillesArticles = require('./backend/routes/familles-articles'),
    articlesFacturation = require('./backend/routes/articles-facturation'),
    roles = require('./backend/routes/roles');

var app = express();

var jsonParser = bodyParser.json();
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(morgan('dev')); // log every request to the console
app.enable('strict routing');
app.set('views', __dirname + '/frontend'); // general config for html rendering
app.set('view engine', 'ejs'); // templating engine
app.use(jsonParser);
app.use(express.static(__dirname+ '/frontend'));
app.use(cookieParser());
app.use(expressSession({
    secret : process.env.SESSION_SECRET || 'AideSociale',
    resave:false,
    saveUninitialized : false
}));
app.use(passport.initialize());
app.use(passport.session());
/**
 * Appel�e lors du post du login
 */
passport.use(new passportLocal.Strategy(function(username, password, callback){
    usersApi.usersDao.login(username,password, function(resultat){
        if (!resultat.error){
            var userObject = resultat.datas[0];
            userRolesApi.userRolesDao.fetchUserRolesByUserId(userObject, function(resultat){
                if (!resultat.error){
                    userObject = resultat.datas;
                    console.log(userObject);
                    callback(null, userObject);
                } else {
                    callback(resultat.message, null);
                }
            });
        } else {
            callback(resultat.message, null);
        }
    });
}));
passport.serializeUser(function(user, callback){
    callback(null, user);
});
passport.deserializeUser(function(user, callback){
    callback(null, user);
});
var server = http.createServer(app);
app.set('port', process.env.PORT || 4000);
app.post('/api/login', urlencodedParser, passport.authenticate('local',{
    successRedirect: '/',
    failureRedirect: '/login', // see text
    failureFlash: true // optional, see text as well
}));
app.get('/logout', function(request, result){
    request.logout();
    result.redirect('/login');
});
/**
 * Serving HTML :) via EJS
 */
app.get('/', LocalStrategy.ensureAuthenticated, function (req, res) {
    res.render('index', {
        user: req.user
    });
}),
app.get('/login', function (req, res) {
    res.render('login');//login.ejs
});
/**
 * Partials routes handling
 */
app.use('/partials',LocalStrategy.ensureAuthenticated, LocalStrategy.ensureHasAdminPrivilege, partials);
/**********************************************************************************************************************
 * Serving JSON data
 **********************************************************************************************************************/
/**
* Users API
*/
app.get('/api/userRoles/:id', LocalStrategy.ensureAuthenticated,LocalStrategy.ensureHasAdminPrivilege, function(request, result){
    usersApi.usersDao.fetchUserById(request, function(userObjectResponse){
        if (!userObjectResponse.error){
            userRolesApi.userRolesDao.fetchUserRolesByUserId(userObjectResponse.datas[0], function(resultat){
                result.json(resultat);
            });
        } else {
            result.json(userObjectResponse);
        }
    });
});
app.get('/api/users',  LocalStrategy.ensureAuthenticated,LocalStrategy.ensureHasAdminPrivilege, function(request, result){
    usersApi.usersDao.fetchUsers(function(resultat){
        result.json(resultat);
    });
});
app.get('/api/users/:id',  LocalStrategy.ensureAuthenticated,LocalStrategy.ensureHasAdminPrivilege, function(request, result){
    usersApi.usersDao.fetchUserById(request, function(resultat){
        result.json(resultat);
    });
});
app.post('/api/users',  LocalStrategy.ensureAuthenticated,LocalStrategy.ensureHasAdminPrivilege, function(request, result){
    usersApi.usersDao.addUser(request, function(resultat){
        result.json(resultat);
    });
});
app.delete('/api/users/:id',  LocalStrategy.ensureAuthenticated,LocalStrategy.ensureHasAdminPrivilege, function(request, result){
    usersApi.usersDao.deleteUser(request, function(resultat){
        result.json(resultat);
    });
});
app.put('/api/users', LocalStrategy.ensureAuthenticated,LocalStrategy.ensureHasAdminPrivilege,  function(request, result){
    usersApi.usersDao.updateUser(request, function(resultat){
        result.json(resultat);
    });
});
/**
* Roles API
*/
app.use('/api/roles', LocalStrategy.ensureAuthenticated,LocalStrategy.ensureHasAdminPrivilege, roles);
app.use('/api/ressources', LocalStrategy.ensureAuthenticated, LocalStrategy.ensureHasAdminPrivilege, ressources);
app.use('/api/charges', LocalStrategy.ensureAuthenticated, LocalStrategy.ensureHasAdminPrivilege, charges);
app.use('/api/modesPaiement', LocalStrategy.ensureAuthenticated, LocalStrategy.ensureHasAdminPrivilege, modesPaiements);
app.use('/api/modesGestionRessources', LocalStrategy.ensureAuthenticated, LocalStrategy.ensureHasAdminPrivilege, modesGestionRessources);
app.use('/api/typeTiersPayeur', LocalStrategy.ensureAuthenticated, LocalStrategy.ensureHasAdminPrivilege, typesTiersPayeurs);
app.use('/api/famillesArticles', LocalStrategy.ensureAuthenticated, LocalStrategy.ensureHasAdminPrivilege, famillesArticles);
app.use('/api/articlesFacturation', LocalStrategy.ensureAuthenticated, LocalStrategy.ensureHasAdminPrivilege, articlesFacturation);
/***********************************************************************************************************************
 * Mise en ligne du service
 ***********************************************************************************************************************/
server.listen(app.get('port'), function() {
    console.log('\u2714 Express server listening on http://localhost:%d/', app.get('port'));
});
