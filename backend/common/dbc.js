/**
 * Created by yacmed on 04/03/2016.
 */
'use strict'
var mysql = require('mysql');
if (typeof(connection)=='undefined'){
    console.log('instantiating dbConnection')
    var connection = mysql.createConnection({
        host : '127.0.0.1',
        user : 'root',
        password : '',
        database : 'aide-sociale'
    });
}
exports.getConnection = function(){
    if (connection.threadId=='undefined'){
        connection.connect();
    }
    return connection;
};
