/**
 * Created by yacmed on 04/03/2016.
 */
'use strict'
var databaseConnection = require('./../common/dbc');
var dataLog = require('./../common/data-logger');
var mysql = require('mysql');

var RessourcesDao = {
    /**
     * Recup�re la liste des ressources et fait appel � la callback qui prend en param le r�sultat {error, message, datas}
     * @param callback
     */
    fetchRessources: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM ressources WHERE flag=1";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des ressources "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    fetchAllRessources : function (callback) {
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM ressources ORDER BY dateCreation DESC";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des ressources "+err.code
                };
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    /**
     * Recup�re la ressource par son #ID, prend en param�tres la requete HTTP, et la CallBack qui prend en param le r�sultat {error, message, datas}
     * @param request
     * @param callback
     */
    fetchRessourceById: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM ressources WHERE ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing fetchRessource query "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    /**
     * Ajoute la ressource pass�e dans la requete HTTP, et appele la CallBack qui prend en param le r�sultat {error, message, datas}
     * @param request
     * @param callback
     */
    addRessource: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "INSERT INTO ressources SET ?";
        request.body.profilCreation = request.user.username;
        request.body.dateCreation = dateSys;
        var parsedQuery = connexionBDD.query(query, request.body,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing MySQL query "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Ressource Added !",
                    "datas":rows.insertId
                };
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
    },
    /**
     * Supprime la ressource pass�e dans la requete HTTP #ID, et appele la CallBack qui prend en param le r�sultat {error, message}
     * @param request
     * @param callback
     */
    deleteRessource: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE ressources SET flag=0, profilSuppression = ?, dateSuppression= ? WHERE flag=1 AND ??=?";
        var binding = [request.user.username,dateSys, "id",request.params.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing MySQL query"
                };
                console.error(err);
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "ressource supprim\u00e9e "+request.params.id,
                        "datas":request.params.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucune ressource trouv\u00e9e avec l'id : "+request.params.id
                    };
                }
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
    }
}
module.exports = RessourcesDao;