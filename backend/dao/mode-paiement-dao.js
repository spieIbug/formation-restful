/**
 * Created by yacmed on 15/03/2016.
 */
'use strict'
var databaseConnection = require('./../common/dbc');
var dataLog = require('./../common/data-logger');
var mysql = require('mysql');
/**
 * Module de gestion (CRUD) des modes de paiements des tiers payants
 * @type {{}}
 */
var ModePaiementDao = {
    /**
     * Methode utilis�e souvent pour r�cup�rer l'historique pour les champs inactifs
     * @param callback
     */
    fetchAllModesPaiements: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_mode_paiement ORDER BY dateCreation DESC";
        var requeteFormate = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation de l'historique des modes de paiements "+err.code
                };
                console.error(err);


            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
        console.info(requeteFormate.sql);
    },
    /**
     * M�thode utilis� pour renvoyer les donn�es actives.
     * @param callback
     */
    fetchModesPaiements: function(callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_mode_paiement WHERE flag=1";
        var requeteFormate = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des modes de paiements "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
        console.info(requeteFormate.sql);
    },
    /**
     * Recup�re le mode choisi
     * @param request
     * @param callback
     */
    fetchModePaiement: function(request, callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM asoc_mode_paiement WHERE  flag=1 AND ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        var requeteFormate = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing fetchModePaiement query "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
        console.info(requeteFormate.sql);
    },
    /**
     * Ajoute un �l�ment actif dans la base de donn�es
     * @param request
     * @param callback
     */
    addModesPaiement: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "INSERT INTO asoc_mode_paiement SET ?";
        request.body.profilCreation = request.user.username;
        request.body.dateCreation = dateSys;
        var requeteFormate = connexionBDD.query(query, request.body,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing MySQL query "+err.code
                };
                console.error(err);
            } else {
                answer = {
                    "error" : false,
                    "message" : "Mode de paiement Added !",
                    "datas":rows.insertId
                };
            }
            callback(answer);
        });
        console.info(requeteFormate.sql);
    },
    /**
     * Desactive un �l�ment actif dans la base de donn�es
     * @param request
     * @param callback
     */
    deleteModesPaiement: function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE asoc_mode_paiement SET flag=0, profilSuppression = ?, dateSuppression= ? WHERE flag=1 AND ??=?";
        var binding = [request.user.username, dateSys, "id",request.params.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing MySQL query "+err.code
                };
                console.error(err);
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "Mode de paiemennt supprime "+request.params.id,
                        "datas":request.params.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucun mode de foncctionnement n'a \u00e9t\u00e9 trouv\u00e9  "+request.params.id
                    };
                }
            }
            callback(answer);
        });
        dataLog.log(parsedQuery.sql);
        console.info(parsedQuery.sql);
    }
}
module.exports = ModePaiementDao;