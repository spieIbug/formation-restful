/**
 * Created by yacmed on 07/03/2016.
 */
'use strict'
var databaseConnection = require('./../common/dbc');
var dataLogger = require('./../common/data-logger');
var mysql = require('mysql');

var ChargesDao = {
    /**
     * Recup�re la liste des charges et fait appel � la callback qui prend en param le r�sultat {error, message, datas}
     * @param callback
     */
    fetchCharges : function (callback) {
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM charges WHERE flag=1";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des charges "+err.code
                };
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    },
    fetchAllCharges : function (callback) {
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM charges ORDER BY dateCreation DESC";
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "Erreur lors de la recuperation des charges "+err.code
                };
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    }
    /**
     * Recup�re la charge par son #ID, prend en param�tres la requete HTTP, et la CallBack qui prend en param le r�sultat {error, message, datas}
     * @param request
     * @param callback
     */
    ,fetchCharge : function(request,callback){
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "SELECT * FROM charges WHERE ??=?";
        var binding = ["id",request.params.id];
        query = mysql.format(query,binding);
        connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing fetchCharge query "+err.code
                };
            } else {
                answer = {
                    "error" : false,
                    "message" : "Succes",
                    "datas" : rows
                };
            }
            callback(answer);
        });
    }
    /**
     * Ajoute la charge pass�e dans la requete HTTP, et appele la CallBack qui prend en param le r�sultat {error, message, datas}
     * @param request
     * @param callback
     */
    ,addCharge : function(request,callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "INSERT INTO charges SET ?";
        request.body.profilCreation = request.user.username;
        request.body.dateCreation = dateSys;
        connexionBDD.query(query, request.body,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing MySQL query "+err.code
                };
            } else {
                answer = {
                    "error" : false,
                    "message" : "Charge ajout\u00e9e !",
                    "datas":rows.insertId
                };
            }
            callback(answer);
        });
    }
    /**
     * Supprime la charge pass�e dans la requete HTTP #ID, et appele la CallBack qui prend en param le r�sultat {error, message}
     * @param request
     * @param callback
     */
    ,deleteCharge : function(request, callback){
        var dateSys=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        var answer;
        var connexionBDD =  databaseConnection.getConnection();
        var query = "UPDATE charges SET flag=0, profilSuppression = ?, dateSuppression= ? WHERE flag=1 AND ??=?";
        var binding = [request.user.username,dateSys, "id",request.params.id];
        query = mysql.format(query,binding);
        var parsedQuery = connexionBDD.query(query,function(err,rows){
            if(err) {
                answer = {
                    "error" : true,
                    "message" : "error executing MySQL query"
                };
            } else {
                if (rows.affectedRows>0){
                    answer = {
                        "error" : false,
                        "message" : "charge supprim\u00e9e "+request.params.id,
                        "datas":request.params.id
                    };
                } else {
                    answer = {
                        "error" : true,
                        "message" : "Aucune charge trouv\u00e9e avec l'id : "+request.params.id
                    };
                }
            }
            callback(answer);
        });
        dataLogger.log(parsedQuery);
    }
};

module.exports = ChargesDao;
