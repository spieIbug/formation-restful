/**
 * Created by yacmed on 08/03/2016.
 */
app.directive('menu', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/menu.html'
    };
});
app.directive('listeCharge', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/charges.select.html',
        controller: 'ChargesCtrl'
    };
});
app.directive('famillesArticles', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/familles-articles.html',
        controller: 'FamillesArticlesCtrl'
    };
});
app.directive('listeFamillesArticles', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/familles-articles.select.html',
        controller: 'FamillesArticlesCtrl'
    };
});
app.directive('loadingSpinner', function ($http) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="loader unixloader" data-initialize="loader" data-delay="500"><div class="gray-layout"><div class="loading"><img ng-src="imgs/ajax-loader.gif"></div></div></div>',
        link: function (scope, element, attrs) {
            scope.$watch('activeCalls', function (newVal, oldVal) {
                if (newVal == 0) {
                    $(element).hide();
                }
                else {
                    $(element).show();
                }
            });
        }
    };
});