/**
 * Created by Yacine 29/03/2016.
 */
app.controller('AppController', ['$scope', function($scope){

}]);
app.controller('ChargesCtrl', ['$scope', 'ChargesSvc', function($scope, ChargesSvc){
    $scope.charges = [];
    $scope.chargesHistory = [];
    $scope.isHistoryEnabled = false;
    $scope.loadData = function(){
        ChargesSvc.fetchCharges().then(function(response){
            if (!response.data.error){
                $scope.charges = response.data.datas;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.loadData();
    $scope.selectedCharge={};
    $scope.showOrHideAddForm = function(){
        $scope.isAddFormEnabled=!$scope.isAddFormEnabled;
        if ($scope.isAddFormEnabled){
            $('#addForm').addClass('animated pulse');
        }
    }
    $scope.showHistory = function(){
        $scope.isHistoryEnabled=!$scope.isHistoryEnabled;
        if ($scope.isHistoryEnabled){
            ChargesSvc.fetchHistoryCharges().then(function(response){
                if (!response.data.error){
                    $scope.chargesHistory = response.data.datas;
                } else{
                    toastr.error(response.message,'Erreur');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
    }
    $scope.addCharge = function(charge){
        ChargesSvc.saveCharge(charge).then(function(response){
            if (!response.data.error){
                charge.id = response.data.datas;
                $scope.charges.push(charge);
                $scope.nouvelleCharge={};
                $scope.isAddFormEnabled = !$scope.isAddFormEnabled;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error('Erreur',data);
        });
    }
    $scope.showConfirmation = function(charge){
        $scope.selectedCharge = charge;
        $('#confirmationDialog').modal('toggle');
        $('#confirmationDialog').addClass('animated flipInY');
    }
    $scope.deleteCharge = function(charge){
        ChargesSvc.deleteCharge(charge).then(function(response){
            if (!response.data.error){
                $scope.charges.splice($scope.charges.indexOf(charge),1);
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.editCharge = function(charge){
        if (charge.editionMode){
            ChargesSvc.updateCharge(charge).then(function(response){
                if (response.data.error){
                    toastr.error(response.message,'Erreur');
                } else {
                    charge.id  = response.data.datas;
                    toastr.info(response.message,'Informations');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
        charge.editionMode = !charge.editionMode;
    }
}]);
app.controller('RessourcesCtrl', ['$scope', 'RessourcesSvc', function($scope, RessourcesSvc){
    $scope.ressources = [];
    $scope.ressourcesHistory = [];
    $scope.isHistoryEnabled = false;
    $scope.loadData = function(){
        RessourcesSvc.fetchRessources().then(function(response){
            if (!response.data.error){
                $scope.ressources = response.data.datas;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.loadData();
    $scope.selectedRessource={};
    $scope.showOrHideAddForm = function(){
        $scope.isAddFormEnabled=!$scope.isAddFormEnabled;
        if ($scope.isAddFormEnabled){
            $('#addForm').addClass('animated pulse');
        }
    }
    $scope.addRessource = function(ressource){
        RessourcesSvc.saveRessource(ressource).then(function(response){
            if (!response.data.error){
                ressource.id = response.data.datas;
                $scope.ressources.push(ressource);
                $scope.nouvelleRessource={};
                $scope.isAddFormEnabled = !$scope.isAddFormEnabled;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.showHistory = function(){
        $scope.isHistoryEnabled=!$scope.isHistoryEnabled;
        if ($scope.isHistoryEnabled){
            RessourcesSvc.fetchHistoryRessources().then(function(response){
                if (!response.data.error){
                    $scope.ressourcesHistory = response.data.datas;
                } else{
                    toastr.error(response.message,'Erreur');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
    }
    $scope.showConfirmation = function(ressource){
        $scope.selectedRessource = ressource;
        $('#confirmationDialog').modal('toggle');
        $('#confirmationDialog').addClass('animated flipInY');
    }
    $scope.deleteRessource = function(ressource){
        RessourcesSvc.deleteRessource(ressource).then(function(response){
            if (!response.data.error){
                $scope.ressources.splice($scope.ressources.indexOf(ressource),1);
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.editRessource = function(ressource){
        if (ressource.editionMode){
            RessourcesSvc.updateRessource(ressource).then(function(response){
                if (response.data.error){
                    toastr.error(response.message,'Erreur');
                } else {
                    ressource.id  = response.data.datas;
                    toastr.info(response.message,'Informations');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
        ressource.editionMode = !ressource.editionMode;
    }
}]);
app.controller('ModesPaiementsCtrl', ['$scope', 'ModePaiementTiersSvc', function($scope, ModePaiementTiersSvc){
    $scope.modePaiementTiers = [];
    $scope.modePaiementTiersHistory = [];
    $scope.isHistoryEnabled = false;
    $scope.loadData = function(){
        ModePaiementTiersSvc.fetchModesPaiement().then(function(response){
            if (!response.data.error){
                $scope.modePaiementTiers = response.data.datas;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.loadData();
    $scope.selectedMode={};
    $scope.showOrHideAddForm = function(){
        $scope.isAddFormEnabled=!$scope.isAddFormEnabled;
        if ($scope.isAddFormEnabled){
            $('#addForm').addClass('animated pulse');
        }
    }
    $scope.showHistory = function(){
        $scope.isHistoryEnabled=!$scope.isHistoryEnabled;
        if ($scope.isHistoryEnabled){
            ModePaiementTiersSvc.fetchHistoryModesPaiement().then(function(response){
                if (!response.data.error){
                    $scope.modePaiementTiersHistory = response.data.datas;
                } else{
                    toastr.error(response.message,'Erreur');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
    }
    $scope.addMode = function(mode){
        ModePaiementTiersSvc.saveModesPaiement(mode).then(function(response){
            if (!response.data.error){
                mode.id = response.data.datas;
                $scope.modePaiementTiers.push(mode);
                $scope.newMode={};
                $scope.isAddFormEnabled = !$scope.isAddFormEnabled;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.showConfirmation = function(mode){
        $scope.selectedMode = mode;
        $('#confirmationDialog').modal('toggle');
        $('#confirmationDialog').addClass('animated flipInY');
    }
    $scope.deleteMode = function(mode){
        ModePaiementTiersSvc.deleteModesPaiement(mode).then(function(response){
            if (!response.data.error){
                $scope.modePaiementTiers.splice($scope.modePaiementTiers.indexOf(mode),1);
            } else{
                toastr.error(response.message, 'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.editMode= function(mode){
        if (mode.editionMode){
            ModePaiementTiersSvc.updateModesPaiement(mode).then(function(response){
                if (response.data.error){
                    toastr.error(response.message,'Erreur');
                } else {
                    mode.id  = response.data.datas;
                    toastr.info(response.message,'Information');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
        mode.editionMode = !mode.editionMode;
    }
}]);
app.controller('ModesGestionRessourcesCtrl', ['$scope', 'ModeGestionRessourcesSvc', function($scope, ModeGestionRessourcesSvc){
    $scope.modeGestionRessources = [];
    $scope.modeGestionRessourcesHistory = [];
    $scope.isHistoryEnabled = false;
    $scope.loadData = function(){
        ModeGestionRessourcesSvc.fetchModesGestionRessources().then(function(response){
            if (!response.data.error){
                $scope.modeGestionRessources = response.data.datas;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.loadData();
    $scope.selectedMode={};
    $scope.showOrHideAddForm = function(){
        $scope.isAddFormEnabled=!$scope.isAddFormEnabled;
        if ($scope.isAddFormEnabled){
            $('#addForm').addClass('animated pulse');
        }
    }
    $scope.showHistory = function(){
        $scope.isHistoryEnabled=!$scope.isHistoryEnabled;
        if ($scope.isHistoryEnabled){
            ModeGestionRessourcesSvc.fetchHistoryModesGestionRessources().then(function(response){
                if (!response.data.error){
                    $scope.modeGestionRessourcesHistory = response.data.datas;
                } else{
                    toastr.error(response.message,'Erreur');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
    }
    $scope.addMode = function(mode){
        ModeGestionRessourcesSvc.saveModesGestionRessources(mode).then(function(response){
            if (!response.data.error){
                mode.id = response.data.datas;
                $scope.modeGestionRessources.push(mode);
                $scope.newMode={};
                $scope.isAddFormEnabled = !$scope.isAddFormEnabled;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.showConfirmation = function(mode){
        $scope.selectedMode = mode;
        $('#confirmationDialog').modal('toggle');
        $('#confirmationDialog').addClass('animated flipInY');
    }
    $scope.deleteMode = function(mode){
        ModeGestionRessourcesSvc.deleteModesGestionRessources(mode).then(function(response){
            if (!response.data.error){
                $scope.modeGestionRessources.splice($scope.modeGestionRessources.indexOf(mode),1);
            } else{
                toastr.error(response.message, 'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.editMode= function(mode){
        if (mode.editionMode){
            ModeGestionRessourcesSvc.updateModesGestionRessources(mode).then(function(response){
                if (response.data.error){
                    toastr.error(response.message,'Erreur');
                } else {
                    mode.id  = response.data.datas;
                    toastr.info(response.message,'Information');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
        mode.editionMode = !mode.editionMode;
    }
}]);
app.controller('TypeTiersPayeurCtrl', ['$scope', 'TypeTiersPayeurSvc', function($scope, TypeTiersPayeurSvc){
    $scope.famillesArticles = [];
    $scope.famillesArticlesHistory = [];
    $scope.isHistoryEnabled = false;
    $scope.loadData = function(){
        TypeTiersPayeurSvc.fetchDatas().then(function(response){
            if (!response.data.error){
                $scope.famillesArticles = response.data.datas;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.loadData();
    $scope.selectedTypeTiers={};
    $scope.showOrHideAddForm = function(){
        $scope.isAddFormEnabled=!$scope.isAddFormEnabled;
        if ($scope.isAddFormEnabled){
            $('#addForm').addClass('animated pulse');
        }
    }
    $scope.showHistory = function(){
        $scope.isHistoryEnabled=!$scope.isHistoryEnabled;
        if ($scope.isHistoryEnabled){
            TypeTiersPayeurSvc.fetchHistoryDatas().then(function(response){
                if (!response.data.error){
                    $scope.famillesArticlesHistory = response.data.datas;
                } else{
                    toastr.error(response.message,'Erreur');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
    }
    $scope.addData = function(typeTiers){
        TypeTiersPayeurSvc.saveData(typeTiers).then(function(response){
            if (!response.data.error){
                typeTiers.id = response.data.datas;
                $scope.famillesArticles.push(typeTiers);
                $scope.newTypeTiers={};
                $scope.isAddFormEnabled = !$scope.isAddFormEnabled;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.showConfirmation = function(typeTiers){
        $scope.selectedTypeTiers = typeTiers;
        $('#confirmationDialog').modal('toggle');
        $('#confirmationDialog').addClass('animated flipInY');
    }
    $scope.deleteData = function(typeTiers){
        TypeTiersPayeurSvc.deleteData(typeTiers).then(function(response){
            if (!response.data.error){
                $scope.famillesArticles.splice($scope.famillesArticles.indexOf(typeTiers),1);
            } else{
                toastr.error(response.message, 'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.editData= function(typeTiers){
        if (typeTiers.editionMode){
            TypeTiersPayeurSvc.updateData(typeTiers).then(function(response){
                if (response.data.error){
                    toastr.error(response.message,'Erreur');
                } else {
                    typeTiers.id  = response.data.datas;
                    toastr.info(response.message,'Information');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
        typeTiers.editionMode = !typeTiers.editionMode;
    }
}]);
app.controller('FamillesArticlesCtrl', ['$scope', 'FamillesArticlesSvc', function($scope, FamillesArticlesSvc){
    $scope.famillesArticles = [];
    $scope.famillesArticlesHistory = [];
    $scope.isHistoryEnabled = false;
    $scope.loadData = function(){
        FamillesArticlesSvc.fetchDatas().then(function(response){
            if (!response.data.error){
                $scope.famillesArticles = response.data.datas;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
        $('select').select2({
            placeholder: "Choix d'un \u00e9l\u00e9ment",
            allowClear: true
        });
    }
    $scope.loadData();
    $scope.selectedFamilleArticles={};
    $scope.showOrHideAddForm = function(){
        $scope.isAddFormEnabled=!$scope.isAddFormEnabled;
        if ($scope.isAddFormEnabled){
            $('#addForm').addClass('animated pulse');
        }
    }
    $scope.showHistory = function(){
        $scope.isHistoryEnabled=!$scope.isHistoryEnabled;
        if ($scope.isHistoryEnabled){
            FamillesArticlesSvc.fetchHistoryDatas().then(function(response){
                if (!response.data.error){
                    $scope.famillesArticlesHistory = response.data.datas;
                } else{
                    toastr.error(response.message,'Erreur');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
    }
    $scope.addData = function(familleArticles){
        FamillesArticlesSvc.saveData(familleArticles).then(function(response){
            if (!response.data.error){
                familleArticles.id = response.data.datas;
                $scope.famillesArticles.push(familleArticles);
                $scope.newFamilleArticles={};
                $scope.isAddFormEnabled = !$scope.isAddFormEnabled;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.showConfirmation = function(familleArticles){
        $scope.selectedFamilleArticles = familleArticles;
        $('#confirmationDialog').modal('toggle');
        $('#confirmationDialog').addClass('animated flipInY');
    }
    $scope.deleteData = function(familleArticles){
        FamillesArticlesSvc.deleteData(familleArticles).then(function(response){
            if (!response.data.error){
                $scope.famillesArticles.splice($scope.famillesArticles.indexOf(familleArticles),1);
            } else{
                toastr.error(response.message, 'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
    }
    $scope.editData= function(familleArticles){
        if (familleArticles.editionMode){
            FamillesArticlesSvc.updateData(familleArticles).then(function(response){
                if (response.data.error){
                    toastr.error(response.message,'Erreur');
                } else {
                    familleArticles.id  = response.data.datas;
                    toastr.info(response.message,'Information');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
        familleArticles.editionMode = !familleArticles.editionMode;
    }
}]);
app.controller('ArticlesFacturationCtrl', ['$scope', 'ArticlesSvc', function($scope, ArticlesSvc){
    $scope.articlesHistory = [];
    $scope.articles = [];
    $scope.isHistoryEnabled = false;
    $scope.loadArticles = function(){
        ArticlesSvc.fetchDatas().then(function(response){
            if (!response.data.error){
                $scope.articles = response.data.datas;
            } else{
                toastr.error(response.message,'Erreur');
            }
        }).catch(function(data){
            toastr.error(data,'Erreur');
        });
        $('select').select2({
            placeholder: "Choix d'un \u00e9l\u00e9ment",
            allowClear: true
        });
    }
    $scope.loadArticles();
    $scope.showArticlesHistory = function(){
        $scope.isHistoryEnabled=!$scope.isHistoryEnabled;
        if ($scope.isHistoryEnabled){
            ArticlesSvc.fetchHistoryDatas().then(function(response){
                if (!response.data.error){
                    $scope.articlesHistory = response.data.datas;
                } else{
                    toastr.error(response.message,'Erreur');
                }
            }).catch(function(data){
                toastr.error(data,'Erreur');
            });
        }
    }


}]);